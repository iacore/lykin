# lykin

_Symbiosis of SSB, key-value store and web server._

## Introduction

lykin is a tutorial application funded by a Secure Scuttlebutt (SSB) community grant. It is intended to showcase one way in which an SSB application can be written using the [golgi](http://golgi.mycelial.technology) client library. The application is not intended for widespread or longterm use and will not be maintained as such; it is purely for demonstration purposes. Anyone is free to fork the repository and use it as the basis for their own application.

If you'd like to learn how lykin works, or how to build your own simple Scuttlebutt client application, please read the [lykin tutorial series](https://git.coopcloud.tech/glyph/lykin_tutorial). You might also like to watch the [lykin demonstration video](https://www.youtube.com/watch?v=mXMAyBOd4is).

![Screenshot of a web UI with a green navigation topbar, a blue list of peers, an orange list of posts and a yellow post content view.](screenshots/lykin_gui.png)

## Quickstart

Prerequisites: a running instance of go-sbot (see the [installation guide](https://github.com/ssbc/go-ssb#installation)).

Download, build and run lykin.

```
git clone https://git.coopcloud.tech/glyph/lykin.git
cd lykin
cargo build --release
RUST_LOG=lykin=info ./target/release/lykin
```

Open `localhost:8000` in your browser.

**Note**: by default, lykin attempts to connect to the go-sbot instance on port 8021. Set the `GO_SBOT_PORT` environment variable if you wish to use another port.

## Features

lykin presents an email inbox-like UI and functions as a Scuttlebutt reader application. It allows the user to subscribe to peers and read the root posts of those peers. Individual posts can be marked as read or unread.

A summary of the features:

 - Subscribe to a peer (follow)
 - Unsubscribe from a peer (unfollow)
 - List subscribed peers
 - List root posts for each peer
 - Mark a post as read
 - Mark a post as unread
 - Fetch new posts

## Design

lykin has been built with the following components:

 - [tera](https://crates.io/crates/tera)   : templating engine
 - [rocket](https://crates.io/crates/rocket/0.5.0-rc.1) : web server
 - [golgi](https://crates.io/crates/golgi)  : Scuttlebutt client library
 - [sled](https://crates.io/crates/sled)   : transactional embedded database (key-value store)
 - [sbot](https://github.com/cryptoscope/ssb)   : Scuttlebutt server (in our case, go-ssb)

## Documentation

This project is documented in the form of code comments and Rust doc comments. In order to generate and read the doc comments in a web browser, run the following command in the lykin repo:

`cargo doc --open --no-deps`

## Logging

lykin emits logs at three levels: `debug`, `info` and `warn`. The log level can be set using the `RUST_LOG` environment variable.

For example, emit only `info` logs:

`RUST_LOG=lykin=info ./target/release/lykin`

It is also possible to emit logs solely from a single module of lykin.

For example, emit only `warn` logs from the `db` module of lykin:

`RUST_LOG=lykin::db=warn cargo run`

## Thanks

I am grateful to the Butts who voted to fund this work, all contributors to the SSBC and Erick Lavoie in particular - both for partially funding this work and for developing and overseeing the community grant process.

## Contact

I can be reached via email: [glyph@mycelial.technology](mailto:glyph@mycelial.technology) or Scuttlebutt: `@HEqy940T6uB+T+d9Jaa58aNfRzLx9eRWqkZljBmnkmk=.ed25519`.

## License

MIT.
