#![warn(missing_docs)]
#![doc = include_str!("../README.md")]

mod db;
mod routes;
mod sbot;
mod task_loop;
mod utils;

use std::env;

use async_std::channel;
use log::{info, warn};
use rocket::{
    fairing::AdHoc,
    fs::{relative, FileServer},
    launch, routes,
};
use rocket_dyn_templates::Template;
use xdg::BaseDirectories;

use crate::{db::Database, routes::*, task_loop::Task};

#[launch]
async fn rocket() -> _ {
    env_logger::init();

    // Retrieve and store the public key of the local sbot.
    match sbot::whoami().await {
        Ok(id) => info!("Public key of the local sbot instance: {}", id),
        Err(e) => warn!("Failed to retrieve the public key of the local sbot instance (`whoami` RPC call failed). Please ensure the sbot is running before trying again. Error details: {}", e)
    }

    // Create the key-value database.
    let xdg_dirs = BaseDirectories::with_prefix("lykin").unwrap();
    let db_path = xdg_dirs
        .place_config_file("database")
        .expect("cannot create database directory");
    let db = Database::init(&db_path);
    let db_clone = db.clone();

    // Create a message passing channel.
    let (tx, rx) = channel::unbounded();
    let tx_clone = tx.clone();

    // Spawn the task loop.
    info!("Spawning task loop");
    task_loop::spawn(db_clone, rx).await;

    info!("Launching web server");
    rocket::build()
        .manage(db)
        .manage(tx)
        .mount(
            "/",
            routes![
                home,
                delete_post,
                download_latest_posts,
                mark_post_read,
                mark_post_unread,
                post,
                posts,
                subscribe_form,
                unsubscribe_form,
            ],
        )
        .mount("/", FileServer::from(relative!("static")))
        .attach(Template::fairing())
        .attach(AdHoc::on_shutdown("cancel task loop", |_| {
            Box::pin(async move {
                tx_clone.send(Task::Cancel).await.unwrap();
            })
        }))
}
